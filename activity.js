db.singleroom.insertOne(
		{
			"name" : "single",
			"accomodates" : 2.0,
			"price" : 1000.0,
			"description" : "A simple room with all the basic necessities",
			"room_available" : 10.0,
			"isAvailable" : false
		}
	
	)

// insert

	db.multipleroom.insertMany([
	{
			"name" : "queen",
			"accomodates" : 4,
			"price" : 4000,
			"description" : "A room  with a queen sized bed perfect for a simple gateaway",
			"room_available" : 15,
			"isAvailable" : false
	},
	{
			"name" : "double",
			"accomodates" : 3.0,
			"price" : 2000.0,
			"description" : "A room fit for a small family going on a vacation",
			"room_available" : 5.0,
			"isAvailable" : false
		}
])

// find multipleroom = double
db.multipleroom.findOne({"name" : "double"})

// update one queen room set to 0
db.multipleroom.updateOne({"name" : "queen"}, {$set: {"room_available" :0})

// deleteMany with room available 0
db.users.deleteMany({"room_available" : 0})
