// CRUD
//  Stand for create, read, update and delete

// CREATE
	// allows us to create documents under a collection or a create a collection if it does not exist yet.
	
	// Syntax
	// db.collections.insertOne()
		// allows to insert or create one
		// collections - stands  for collection name
	db.users.insertOne(
	{
		"firstName": "Tony",
		"lastName": "Stark",
		"username": "iAmIronMan",
		"email": "iloveyou3000@gmail.com",
		"password": "starkIndustries",
		"isAdmin": true
	}
)

	// db.collections.insertMany()
		// allows us to insert  or  create two or more documents.

	db.users.insertMany([
		{
			"firstName": "Pepper",
		"lastName": "Potts",
		"username": "rescueArmor",
		"email": "pepper@gmail.com",
		"password": "whereisTonyAgain",
		"isAdmin": false
		},
		{
			"firstName": "Steve",
		"lastName": "Rogers",
		"username": "theCaptain",
		"email": "captAmerica@gmail.com",
		"password": "iCanLiftMjolnirtoo",
		"isAdmin": false
		},
		{
			"firstName": "Thor",
		"lastName": "Odinson",
		"username": "mightyThor",
		"email": "ThorNotLoki@gmail.com",
		"password": "iAmWorthytoo",
		"isAdmin": false
		},
		{
			"firstName": "Loki",
		"lastName": "Odinson",
		"username": "godOfMischief",
		"email": "loki@gmail.com",
		"password": "iamReallyLiki",
		"isAdmin": false
		}
	])

// mini-activity
	
db.courses.insertMany([
{	
	"name": "Javascript",
	"price": 3500,
	"description": "Learn Javascript in a week!",
	"isActive": true
},

{
	"name": "HTML",
	"price": 1000,
	"description": "Learn Basic HTML in a 3 days!",
	"isActive": true
},
{

	"name": "CSS",
	"price": 2000,
	"description": "Make you website fancy, learn CSS now",
	"isActive": true
}

])

// READ
	// Allows us to retrieve data
	// it needs a query or filter s to specify the document we are retrieveing.

// Syntax:
	// db.collections.find()
		// allows us to retrieve ALL  documents in the collection.
db.users.find();

// sytntax:
	// db.collections.findOne({})
		// allows us to find the document  that matches our criteria

db.users.find({"isAdmin" : false});

// Syntax:
	// db.collections.findOne({})
		// allows to find the first document.
db.users.findOne({});

// Syntax:
	// db.collections.find({,})
		// allows us to  find the document that satisfy all criteria.

db.users.find({"lastName":"Odinson", "firstName" : "Loki"});


// UPDATE 
	// allows to update documents.
	// also use criteria or filter.
	// $set operator
	// REMINDER: Updates are permanent and can't be rolled back.

// Syntax:
	// db.collections.updateOne({criteria: value}, {$set: {}})
		// allows us to update one document that satisfy the criteria
db.users.updateOne({"lastName" : "Potts"}, {$set: {"lastName" : "stark"}});

// Syntax;
	// db.collections.updateMany({criteria: value}, {$set: {fieldTobeUpdated: updatedValue}});
		// allows us  to update  ALL document that satisfy the criteria

db.collections.updateMany({"lastName": "Odinson"}, {$set: {"isAdmin": true}});

// Syntax:


db.users.updateOne({}, {$set: {"email" : "starkindustries@mail.com"}});



db.courses.updateOne({"name": "Javascript"}, {$set: {"isActive": false}});

db.courses.updateMany({}, {$set: {"enrolless" : 10}});

db.users.deleteOne({"isAdmin" : false});

db.users.deleteMany({"lastName" : "Odinson"})

db.users.deleteOne({});

db.users.deleteMany({});  delete all


